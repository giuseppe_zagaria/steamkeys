<?php
/*
 * Template Name: Mostra Chiavi
 */
get_header(); ?>

<?php get_template_part('module_pageTit'); ?>
<?php get_template_part('module_panList'); ?>

<div class="section siteContent">
<div class="container">
<div class="row">

<div class="col-md-12 mainSection" id="main" role="main">

    <?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

    <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <?php do_action( 'ligthning_entry_body_before' ); ?>
    <div class="entry-body">
	<?php 
/*****CODICE Del PARTNER CON FOTO PDF E ROBA CARICATA DA LUI ****/
global $current_user;
get_currentuserinfo();
//print_r($current_user);
if (($current_user->user_nicename) == "acc" ){

	echo '<a href="/upload">UPLOAD</a><br>';
}
//echo 'Username: ' . $current_user->user_login . "\n";//DEBUG USER

$dir = $_SERVER['DOCUMENT_ROOT']."/wp-content/uploads/user_uploads/ACC";

// Sort in ascending order - this is default
$a = scandir($dir);
$pdf_url = array();
$images_url = array();

foreach ( $a as $b){

  $floc = $_SERVER['DOCUMENT_ROOT'].'/wp-content/uploads/user_uploads/ACC/'.$b;

	if (is_file($floc)){
	if(mime_content_type($floc) == 'image/gif' || mime_content_type($floc) == 'image/jpeg' || mime_content_type($floc) == 'image/png'){
 

   //echo '<br><a href="/wp-content/uploads/user_uploads/ACC/'.$b.'">'.$b.'</a><br>';
         array_push($images_url,'/wp-content/uploads/user_uploads/ACC/'.$b);

	}elseif(mime_content_type($floc) == 'application/pdf'){

		//echo '<br><a href="/wp-content/uploads/user_uploads/ACC/'.$b.'">'.$b.'</a><br>';
        //echo 'da buttare nella sezione PDF';	
        array_push($pdf_url,'/wp-content/uploads/user_uploads/ACC/'.$b);
	}

}
}

//GALLERIA IMMAGINI
?>

<h1>IMAGE GALLERY</h1>
<div class="container">
	<div class="row">
	<?php 
	foreach ( $images_url as $im){
		echo '<div class="col-md-3 col-sm-4 col-xs-6"><a href="#"><img class="thumbnail img-responsive" src="'.$im.'" /></a></div>';
	}
	?>
    </div>
</div>

<div id="myModal" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog">
  <div class="modal-content">
	<div class="modal-body">
		
	</div>
	<div class="modal-footer">
		<button class="btn btn-default" data-dismiss="modal">Close</button>
	</div>
   </div>
  </div>
</div>
<script>
jQuery('.thumbnail').click(function(){
  	jQuery('.modal-body').empty();
  	var title = jQuery(this).parent('a').attr("title");
  	jQuery('.modal-title').html(title);
  	jQuery(jQuery(this).parents('div').html()).appendTo('.modal-body');
  	jQuery('#myModal').modal({show:true});
});
</script>

<!--FINE GALLERIA IMMAGINI-->

<h1>DOCUMENTATION</h1>
<?php 
	foreach ( $pdf_url as $pd){
		echo '<br><a targe="_blank" href="'.$pd.'"><h3>'.strtoupper(substr($pd, 42, -4)).'</h3></a><br>';
	}
?>
<!--SEZIONE VIDEO-->
<h1>VIDEO</h1>
<?php
global $wpdb;
//print_r($results);
$videos = $wpdb->get_results( "SELECT * FROM video_link WHERE id_user = $current_user->ID", OBJECT );
foreach ($videos as $vid){
?>
<iframe width="560" height="315" src="<?=str_replace('watch?v=', '/embed/', $vid->url);?>" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
<?php
}
?>
<!--FINE SEZIONE VIDEO-->
<button onclick="goBack()">Go Back</button>

<script>
function goBack() {
    window.history.back();
}
</script>
    <?php the_content(); ?>
    </div>
	<?php
	$args = array(
		'before'           => '<nav class="page-link"><dl><dt>Pages :</dt><dd>',
		'after'            => '</dd></dl></nav>',
		'link_before'      => '<span class="page-numbers">',
		'link_after'       => '</span>',
		'echo'             => 1
		);
	wp_link_pages( $args ); ?>
    </div><!-- [ /#post-<?php the_ID(); ?> ] -->

	<?php endwhile; ?>

</div><!-- [ /.mainSection ] -->

</div><!-- [ /.row ] -->
</div><!-- [ /.container ] -->
</div><!-- [ /.siteContent ] -->
<?php get_footer(); ?>