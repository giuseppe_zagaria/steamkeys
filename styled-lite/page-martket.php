<?php
/*
 * Template Name: Inserisci Chiavi
 */

get_header();
global $current_user;
get_currentuserinfo();	  
	global $wpdb;
?>
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<!-- Additional CSS Themes file - not required-->
<link rel="stylesheet" href="/home/easy-autocomplete.themes.min.css"> 
<div id="wrapper">
 
    <h1 class="entry-title">
      <?php the_title(); ?>
    </h1>
     <div id="contentwrapper" class="animated fadeIn">
    <div id="content">
      <?php //if (have_posts()) : while (have_posts()) : the_post(); ?>
      <div class="post" id="post-<?php the_ID(); ?>">
        <div class="entry">
<?
$skeys = $wpdb->get_results( "SELECT game,owner_id,idkey FROM entity", OBJECT );

echo "<table id='entita' style='text-align:center;' class='display dataTable no-footer'><thead><th style='text-align:center;'>OWNER</th><th style='text-align:center;'>GAME NAME</th><th></th></tr></thead>";
echo "<tbody>";
foreach ($skeys as $skey){
	echo "<tr>";
   echo "<td><a target='_blank' href='/home/user-page/?owner_id=".$skey->owner_id."'>".$skey->owner_id."</a></td><td>".$skey->game."</td><td><a href='/home/exchange/?idkey=$skey->idkey'><img alt='exchange' src='http://www.aac-school.eu/wp-content/plugins/nmedia-user-file-uploader/images/delet.png'></a></td>";
   echo "</tr>";
}
echo "</tbody></table>";
?>
          <?php the_content(); ?>
          <?php //edit_post_link(); ?>
          <?php //wp_link_pages(array('before' => '<p><strong>'. esc_html__( 'Pages:', 'styled-lite' ) .'</strong> ', 'after' => '</p>', 'next_or_number' => 'number')); ?>
          <?php //comments_template(); ?>
        </div>
      </div>
      <?php //endwhile; endif; ?>
    </div>
    <?php //get_sidebar(); ?>
  </div>
</div>
<?php //get_footer(); ?>

<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script>
jQuery(document).ready(function() {
    jQuery('#entita').DataTable();
} );
</script>